package com.example.oliver.csimplifyit_pr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by OLIVER on 16-04-2016.
 */
public class Drawer_List_Adapter extends ArrayAdapter {
    private LayoutInflater layoutInflater;
    List<Drawer_List_Model> data;
    Context context;

    public Drawer_List_Adapter(Context context, List<Drawer_List_Model> data) {
        super(context, 0, data);
        this.data = data;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Drawer_List_Model drawer_list_model = new Drawer_List_Model();
        drawer_list_model = data.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.custom_row, parent, false);

        }
        TextView textView = (TextView) convertView.findViewById(R.id.drawer_list_text_id);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.drawer_list_icon_id);
        textView.setText(drawer_list_model.getTitle());
        imageView.setImageResource(drawer_list_model.getIconId());
        return convertView;
    }
}
