package com.example.oliver.csimplifyit_pr.content_providers;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.example.oliver.csimplifyit_pr.models.MessageModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageProvider {

    private Context context;
    private List<MessageModel> messageModelList;

    public MessageProvider(Context context) {
        this.context = context;
    }


    public List<MessageModel> getSMS() {

        Uri uri = Uri.parse("content://sms/inbox");

        Cursor cursor = context.getContentResolver().query(uri, new String[]{"_id", "type", "address", "body", "date"}, null, null, null);

        int i = 0;
        messageModelList = new ArrayList<>();
        while (cursor.moveToNext()) {
            MessageModel messageModel = new MessageModel();
            messageModel.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("_id"))));
            messageModel.setContacts(cursor.getString(cursor.getColumnIndex("address")));
            messageModel.setDate(getDateFormate(cursor.getString(cursor.getColumnIndex("date"))));
            messageModel.setBody(cursor.getString(cursor.getColumnIndexOrThrow("body")));
            messageModelList.add(messageModel);
        }

        return messageModelList;
    }

    public Date getDateFormate(String dateString) {
        Date date = new Date();
        String.format(dateString, date);


        return date;
    }
}
