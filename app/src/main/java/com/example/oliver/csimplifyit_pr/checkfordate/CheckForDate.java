package com.example.oliver.csimplifyit_pr.checkfordate;

import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sumit on 4/22/2016.
 */
public class CheckForDate {
    static String regex1 = "\\d\\d-\\d\\d-\\d\\d\\d\\d";
    static String regex2 = "\\d\\d\\d\\d-\\d\\d-\\d\\d";
    static String regex3 = "\\d-\\d-\\d\\d\\d\\d";
    static String regex4 = "\\d\\d\\d\\d-\\d-\\d";
    static String regex5 ="\\d\\d/\\d\\d/\\d\\d\\d\\d";
    static String regex6 ="\\d\\d\\d\\d/\\d\\d/\\d\\d";
    static String regex7 ="\\d/\\d/\\d\\d\\d\\d";
    static String regex8 = "\\d\\d\\d\\d/\\d/\\d";
    static String regex9 = "\\d\\d.\\d\\d.\\d\\d\\d\\d";
    static  String regex10 ="\\d.\\d.\\d\\d\\d\\d";
    static String regex11 ="\\d\\d\\d\\d.\\d\\d.\\d\\d";
    static  String regex12 ="\\d\\d\\d\\d.\\d.\\d";


    static String regex = regex1 + "|" + regex2 + "|" + regex3 + "|" + regex4+ "|" + regex5+ "|" + regex6+ "|" + regex7+
            "|" + regex8+ "|" + regex9+ "|" + regex10+ "|" + regex11+ "|" + regex12;
    static String input = "";
    static int start = 0;
    static int end = 0;

    public static boolean isContainDate(String body) {
        input = body;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            start = matcher.start();
            end = matcher.end();

        }
        Log.d("sumit", " Date :" + input.substring(start, end));

        return true;
    }
}