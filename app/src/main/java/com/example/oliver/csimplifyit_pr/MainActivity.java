package com.example.oliver.csimplifyit_pr;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import com.example.oliver.csimplifyit_pr.adapters.OverdueAdapter;
import com.example.oliver.csimplifyit_pr.adapters.TodayAdapter;
import com.example.oliver.csimplifyit_pr.adapters.UpcomingAdapter;
import com.example.oliver.csimplifyit_pr.checkfordate.CheckForDate;
import com.example.oliver.csimplifyit_pr.content_providers.MessageProvider;
import com.example.oliver.csimplifyit_pr.models.MessageModel;

import java.util.List;


public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    List<MessageModel> messageModelList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        /*message provider start */
        MessageProvider messageProvider = new MessageProvider(this);
        messageModelList = messageProvider.getSMS();
        /* message provider end */
        Log.d("sumit"," "+CheckForDate.isContainDate("g2016.21.12 fw"));
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_fragment_id);
        drawerFragment.setUp(R.id.navigation_fragment_id, (DrawerLayout) findViewById(R.id.drawer_layout_id), toolbar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.today: {
                ListView listView = (ListView) findViewById(R.id.todayList_id);
                MessageProvider messageProvider = new MessageProvider(getApplicationContext());
                TodayAdapter todayAdapter = new TodayAdapter(getApplicationContext(), messageProvider.getSMS());
                listView.setAdapter(todayAdapter);

                break;

            }
            case R.id.overdue: {
                ListView listView = (ListView) findViewById(R.id.todayList_id);
                MessageProvider messageProvider = new MessageProvider(getApplicationContext());
                OverdueAdapter overdueAdapter = new OverdueAdapter(getApplicationContext(), messageProvider.getSMS());
                listView.setAdapter(overdueAdapter);

                break;

            }
            case R.id.upcoming: {
                ListView listView = (ListView) findViewById(R.id.todayList_id);
                MessageProvider messageProvider = new MessageProvider(getApplicationContext());
                UpcomingAdapter upcomingAdapter = new UpcomingAdapter(getApplicationContext(), messageProvider.getSMS());
                listView.setAdapter(upcomingAdapter);

                break;

            }


        }
        return true;
    }
}
