package com.example.oliver.csimplifyit_pr;

/**
 * Created by OLIVER on 16-04-2016.
 */
public class Drawer_List_Model {
    int iconId;
    String title;

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconId() {
        return iconId;
    }

    public String getTitle() {
        return title;
    }

}
