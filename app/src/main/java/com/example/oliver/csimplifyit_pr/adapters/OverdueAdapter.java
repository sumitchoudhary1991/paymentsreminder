package com.example.oliver.csimplifyit_pr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.oliver.csimplifyit_pr.R;
import com.example.oliver.csimplifyit_pr.models.MessageModel;

import java.util.List;

/**
 * Created by sumit on 4/22/2016.
 */

public class OverdueAdapter extends ArrayAdapter {
    private Context context;
    List<MessageModel> list;

    public OverdueAdapter(Context context, List<MessageModel> list) {
        super(context, 0, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageModel model = list.get(position);
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.overduerow,parent,false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.textView);
        textView.setText(model.getBody());



        return convertView;
    }
}
